// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_SBGameMode.generated.h"

UCLASS(minimalapi)
class ATPS_SBGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_SBGameMode();

	void PlayerCharacterDead();
};



