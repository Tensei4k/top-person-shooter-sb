// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS_SB/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPS_SB/FuncLibrary/WeaponDefault.h"

#include "TPS_SBGameInstance.generated.h"

UCLASS()
class TPS_SB_API UTPS_SBGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:	
	//����� �������� � ������ � ���� ���������, ������ ������������ ������ � �������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* DropItemInfoTable = nullptr;
	//���� ����� �������� ��� �������, ������� ������� ���������  ���������
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);

};
