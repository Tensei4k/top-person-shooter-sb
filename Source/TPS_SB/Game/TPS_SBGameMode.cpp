// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SBGameMode.h"
#include "TPS_SBPlayerController.h"
#include "TPS_SB/Character/TPS_SBCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_SBGameMode::ATPS_SBGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_SBPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	// we commit this because was bug
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_Character"));
	//if (PlayerPawnBPClass.Class != nullptr)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
}

void ATPS_SBGameMode::PlayerCharacterDead()
{

}
