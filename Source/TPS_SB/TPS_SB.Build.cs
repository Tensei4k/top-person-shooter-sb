// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPS_SB : ModuleRules
{
	public TPS_SB(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "PhysicsCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "Slate" });

    }
}
