// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS_SB/Interface/TPS_SB_IGameActor.h"
#include "TPS_SB/FuncLibrary/StateEffect.h"
#include "TPS_SB_EnviromentStructure.generated.h"

UCLASS()
class TPS_SB_API ATPS_SB_EnviromentStructure : public AActor, public ITPS_SB_IGameActor
{
	GENERATED_BODY()
public:

	ATPS_SB_EnviromentStructure();
protected:
	virtual void BeginPlay() override;
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:

	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UStateEffect*> GetAllCurrentEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UStateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UStateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UStateEffect* NewEffect);
	void AddEffect_Implementation(UStateEffect* NewEffect) override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UStateEffect*> Effects;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	FVector SpawnEffectLocation = FVector(0.f,0.f,200.f);*/

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	FVector OffsetEffect = FVector(0);

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteAddParticle);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteAddParticle);

	UFUNCTION()
		void SwitchEffect(UStateEffect* Effect, bool bIsAdd);
};


