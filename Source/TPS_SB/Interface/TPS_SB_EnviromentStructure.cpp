// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_SB_EnviromentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"


ATPS_SB_EnviromentStructure::ATPS_SB_EnviromentStructure()
{
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
}

void ATPS_SB_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
}

void ATPS_SB_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EPhysicalSurface ATPS_SB_EnviromentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(UStaticMeshComponent::StaticClass());
	if (MyMesh)
	{
		UMaterialInterface* MyMaterial = MyMesh->GetMaterial(0);
		if (MyMaterial)
		{
			Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UStateEffect*> ATPS_SB_EnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_SB_EnviromentStructure::RemoveEffect_Implementation(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void ATPS_SB_EnviromentStructure::AddEffect_Implementation(UStateEffect* NewEffect) //was UParticleSystemComponent*
{
	Effects.Add(NewEffect);
	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}

	//UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	//Effects.Add(NewEffect);
	//return 	UGameplayStatics::SpawnEmitterAttached(NewEffect->ParticleEffect, MyMesh, NewEffect->NameOfBone, SpawnEffectLocation,
	//	FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
}

void ATPS_SB_EnviromentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);

	}
}

void ATPS_SB_EnviromentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPS_SB_EnviromentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteAddParticle)
{
	ExecuteEffectAdded_Multicast(ExecuteAddParticle);
}

void ATPS_SB_EnviromentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteAddParticle)
{
	UTypes::ExecuteEffectAdded(ExecuteAddParticle, this, OffsetEffect, NAME_None);
}

void ATPS_SB_EnviromentStructure::SwitchEffect(UStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttack = Effect->NameBone;
			FVector Locat = FVector(0);
			USceneComponent* MySceneComp = GetRootComponent();
			if (MySceneComp)
			{
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MySceneComp, NameBoneToAttack, Locat, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsfind = false;
			if (ParticleSystemEffects.Num() > 0)
			{
				while (i < ParticleSystemEffects.Num() && !bIsfind)
				{
					if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsfind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}

		}

	}
}

bool ATPS_SB_EnviromentStructure::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPS_SB_EnviromentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPS_SB_EnviromentStructure, Effects);
	DOREPLIFETIME(ATPS_SB_EnviromentStructure, EffectAdd);
	DOREPLIFETIME(ATPS_SB_EnviromentStructure, EffectRemove);
}