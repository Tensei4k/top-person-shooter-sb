// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TPS_SB/FuncLibrary/StateEffect.h"
#include "TPS_SB/FuncLibrary/Types.h"
#include "TPS_SB_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPS_SB_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TPS_SB_API ITPS_SB_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//UFUNCTION(BlueprintCallable,BlueprintImplementableEvent, Category = "Interface")
	//void AvailableForEffectsBP();
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interface")
	//bool AviableForEffects();

	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UStateEffect*> GetAllCurrentEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	 void RemoveEffect(UStateEffect* RemoveEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	 void AddEffect(UStateEffect* NewEffect);

	//Inventry
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
