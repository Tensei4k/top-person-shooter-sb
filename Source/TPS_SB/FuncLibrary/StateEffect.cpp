// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect.h"
#include "TPS_SB/Character/CharacterHealthComponent.h"
#include "TPS_SB/Interface/TPS_SB_IGameActor.h"
#include "TPS_SB/Character/TPS_SBCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "TPS_SB/Character/TPS_SBCharacter.h"
#include "Net/UnrealNetwork.h"

bool UStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	MyActor = Actor;
	NameBone = NameBoneHit;
	ITPS_SB_IGameActor* MyInterface = Cast<ITPS_SB_IGameActor>(MyActor);
	if (MyInterface)
	{
		 MyInterface->Execute_AddEffect(MyActor, this);
	}
	return true;
}

void UStateEffect::DestroyObject()
{
	ITPS_SB_IGameActor* MyInterface = Cast<ITPS_SB_IGameActor>(MyActor);
	if (MyInterface)
	{
		MyInterface->Execute_RemoveEffect(MyActor, this);
	}

	MyActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}

}

bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();

	return true;
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	
	Super::DestroyObject();
	/*if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}*/
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (MyActor)
	{
		UHealthComponent* MyHealthComponent = Cast<UHealthComponent>(MyActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (MyHealthComponent)
		{
			MyHealthComponent->ChangeHealthValue_OnServer(Power);
		}
	}
	DestroyObject();
	//GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteOnce::DestroyObject, RateTime, false);
	
}

bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Evecute, RateTime, true);
	}
	// this function in lesson on multicast but from comment
	 //if (ParticleEffect)
	 //{
		// FName NameBoneToAttack = NameBoneHit;
		// FVector Loc = FVector(0);
		// USceneComponent* MyMesh = Cast<USceneComponent> (MyActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		// if (MyMesh)
		// {
		//	 ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyMesh, NameBoneToAttack, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		// }
		// else
		// {
		//	 ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, MyActor->GetRootComponent(), NameBoneToAttack, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		// }

	 //}
	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	Super::DestroyObject();
	/*if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}*/
}

void UStateEffect_ExecuteTimer::Evecute()
{
	if (MyActor)
	{
		UHealthComponent* MyHealthComponent = Cast<UHealthComponent>(MyActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (MyHealthComponent)
		{
			MyHealthComponent->ChangeHealthValue_OnServer(Power);
		}
	}
}

void UStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UStateEffect, NameBone);

}