// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "TPS_SB/Inventory/InventoryComponent.h"
#include <TPS_SB/FuncLibrary/StateEffect.h>
#include "Net/UnrealNetwork.h"

int32 DebugWeaponShow = 1;
FAutoConsoleVariableRef CVarWeaponShow(TEXT("TPS_SB.DebugWeapon"), DebugWeaponShow, TEXT("Draw Debug for Weapon"), ECVF_Cheat);

AWeaponDefault::AWeaponDefault()
{
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit(); 
}

void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//This function tell other function work only on server
	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		DispersionTick(DeltaTime);
		ShellDropTick(DeltaTime);
		ClipDropTick(DeltaTime);
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{

		if (WeaponFiring &&GetWeaponRound() > 0 && !WeaponReloading)
		{
			//�������� �������� ��������, ���������� � Fire
			if (FireTimer < 0.0f)
			{
					Fire();
			}
			//���� ��������� �������, ���������� ���������� �������� � ������ ����
			else
			{
				FireTimer -= DeltaTime;
			}
		}

}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		//������ �������������
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouilRadiuceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}
		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (!ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: Max = %f. MIN = %f. Current = %f."), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.f)
		{
			DropShellFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset,
				WeaponSetting.ShellBullets.DropMeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime,
				WeaponSetting.ShellBullets.ImulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
		}
		else
			DropShellTimer -= DeltaTime;
		
	}
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.f)
		{
			DropClipFlag = false;
			InitDropMesh_OnServer(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset,
				WeaponSetting.ClipDropMesh.DropMeshImpulseDir, WeaponSetting.ClipDropMesh.DropMeshLifeTime,
				WeaponSetting.ClipDropMesh.ImulseRandomDispersion, WeaponSetting.ClipDropMesh.PowerImpulse, WeaponSetting.ClipDropMesh.CustomMass);
		} 
		else
		{
			DropClipTimer -= DeltaTime;
		}
	}

}

//��� �������� �������� ��� � ��� ��� ������ ���, �������� �������
void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

	UpdateStateWeapon_OnServer(EMovementState::Run_State);
	
}
//��������� �����, ����� ��� ������� �� ��� CheckWeaponCanFire ����� ���� ����� ������, �� � ������ ��� ���� �������, ��� �� ��������
void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
		WeaponFiring = false;
		FireTimer = 0.01f;
	
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}
//��� ����� ������� ��� ������ ����, ����� ��������� ����� ��� ��������
FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	//On Server by WeaponFire bool
	//��� � �����, ���� �� ����� ��� ���� �� � ���� ��������� WeaponAiming
	UAnimMontage* AnimToPlay = nullptr;
	if (Aiming)
	{
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	}
	else
	{
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharFire;
	}
	if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire)
	{
		AnimWeaponStart_Multicast(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	}

	//�������� ���� �� ���
	if (WeaponSetting.ShellBullets.DropMesh)
	{
		//�������� �� ������
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh_OnServer(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset,
						 WeaponSetting.ShellBullets.DropMeshImpulseDir,WeaponSetting.ShellBullets.DropMeshLifeTime,
						 WeaponSetting.ShellBullets.ImulseRandomDispersion,WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}
	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispertionByShot();

	OnWeaponFireStart.Broadcast(AnimToPlay);

	FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);

	int8 NumberProjectile = GetNumberProjectileByShot();
	//�������� ���� �� ��������� ShootLocation
	if (ShootLocation)
	{
		//����� ����
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		
		FVector EndLocation;
		//������� ����� ����� �������� ���� ������, ��� ������ ����������, ��������� ����������, ������� ��������
		for (int8 i = 0; i < NumberProjectile; i++)  //Shotgun
		{
			EndLocation = GetFireEndLocation();


			//�������� ������� TSubclassOf ����
			if (ProjectileInfo.Projectile)
			{
				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix MyMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = MyMatrix.Rotator();
				//������� ����
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();
				//����� ����
				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					//����� ����� ����
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
				
			}
			//���� ��� ������������ �� �����������
			else
			{
				FHitResult Hit;
				TArray<AActor*> Actors;
				//Debug func
				EDrawDebugTrace::Type DebugTrace;
				if (DebugWeaponShow)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DebugTrace = EDrawDebugTrace::ForDuration;
				}
				else
				{
					DebugTrace = EDrawDebugTrace::None;
				}
				//����� ������
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace, ETraceTypeQuery::TraceTypeQuery4,
														false, Actors, EDrawDebugTrace::ForDuration, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.f);

				if (ShowDebug)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
				}

				

				//Trace logic
				//To Do
				//Multicast
				//Multicast ThaceFx need to make ourself
				//Dn't forget FHitResult Hit;TArray<AActor*> Actors; add to multicast func
				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface MySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(MySurfaceType))
					{
						UMaterialInterface* MyMaterial = WeaponSetting.ProjectileSetting.HitDecals[MySurfaceType];
						
						WeaponTraceDecal_Multicast(Hit,MyMaterial);
						/*if (MyMaterial&&Hit.GetComponent())
						{
							
							UGameplayStatics::SpawnDecalAttached(MyMaterial, FVector(20.f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 0.f);
						}*/
					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(MySurfaceType))
					{
						UParticleSystem* MyParticle = WeaponSetting.ProjectileSetting.HitFXs[MySurfaceType];
						WeaponTraceFx_Multicast(Hit, MyParticle);
						/*if (MyParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MyParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.f)));
						}*/
					}
					//WeaponTraceSoundEffectDamage_Multicast(Hit, ProjectileInfo, MySurfaceType);
					//if (WeaponSetting.ProjectileSetting.HitSound)
					//{
					//	UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
					//}

					//UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, MySurfaceType);
					////UStateEffect* NewEffect = NewObject<UStateEffect>(Hit.GetActor(), FName("Effect"));

					//UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage,Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
				}
			}
		}
		
	}

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
		{
			InitReload();
		}
	}
}

void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	//����� ������� �������
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispertionWeapon.Aim_StateDispertionAimMax;
		CurrentDispersionMin = WeaponSetting.DispertionWeapon.Aim_StateDispertionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispertionWeapon.Aim_StateDispertionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispertionWeapon.Aim_StateDispertionAimReduction;
		Aiming = true;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispertionWeapon.AimWalk_StateDispertionAimMax;
		CurrentDispersionMin = WeaponSetting.DispertionWeapon.AimWalk_StateDispertionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispertionWeapon.AimWalk_StateDispertionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispertionWeapon.AimWalk_StateDispertionAimReduction;
		Aiming = true;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispertionWeapon.Walk_StateDispertionAimMax;
		CurrentDispersionMin = WeaponSetting.DispertionWeapon.Walk_StateDispertionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispertionWeapon.Walk_StateDispertionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispertionWeapon.Walk_StateDispertionAimRecoil;
		Aiming = false;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispertionWeapon.Run_StateDispertionAimMax;
		CurrentDispersionMin = WeaponSetting.DispertionWeapon.Run_StateDispertionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispertionWeapon.Run_StateDispertionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispertionWeapon.Run_StateDispertionAimReduction;
		Aiming = false;
		break;
	case EMovementState::SprintRun_State:
		Aiming = false;
		BlockFire = true;
		SetWeaponStateFire_OnServer(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispertionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispertion() const
{
	float Result = CurrentDispersion;
	return Result;
}
//���������� ������� �� �������
FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispertion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool BShootDirection = false;
	FVector EndLocation = FVector(0.f);
	//�� ���� ���������� ����� ���� ����� ����������
	FVector TmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	if (TmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace,
				GetCurrentDispertion() * PI / 180.0f, GetCurrentDispertion() * PI / 180.0f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}
	//���� ������ ������ � ���������, �������� �������� ��������������� �� ������, ��������� ���
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
		{
			DrawDebugCone(GetWorld(),ShootLocation->GetComponentLocation(),ShootLocation->GetForwardVector(),WeaponSetting.DistanceTrace,
				GetCurrentDispertion() * PI / 180.0f, GetCurrentDispertion() * PI / 180.0f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}
	if (ShowDebug)
	{
		//����������� ���� ������ �������
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//���������� ���� ����� ������ ����
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//����������� ������ ����
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}

	return EndLocation;
}
//������� ������� �������� ����
int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}
//����������� ������
void AWeaponDefault::InitReload()
{
	//OnServer
	WeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime; 

	// old version
	//if (WeaponSetting.ClipDropMesh.DropMesh)
	//{
	//	DropClipFlag = true;
	//	DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
	//}
	//if (WeaponSetting.AnimWeaponInfo.AnimCharReload)
	//{
	//	//���� ��� ������ ��� ���� ������� ��� ������� ���������
	//	OnWeaponReloadStart.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharReload);
	//}
	
	//This cod from lesson but me need to add Stop reload animation to Types
	UAnimMontage* AnimToPlay = nullptr;
	if (Aiming)
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReload;
	else
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReload;

	OnWeaponReloadStart.Broadcast(AnimToPlay);

	UAnimMontage* AnimWeaponToPlay = nullptr;
	if (Aiming)
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
	else
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;

	if (WeaponSetting.AnimWeaponInfo.AnimWeaponReload
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())
	{
		//SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
		AnimWeaponStart_Multicast(AnimWeaponToPlay);
	}


	if (WeaponSetting.ClipDropMesh.DropMesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	//take available ammo from inventory, if we has no inventory, weapon take max round for this weapon
	int8 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;
	//limit ammo to weapon can't reload more max ammo
	if (NeedToReload > AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round += AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}
	////How Much projectile was in magazine when we start reload
	//int32 AmmoNeedTake = AdditionalWeaponInfo.Round;
	//AmmoNeedTake -= AviableAmmoFromInventory;
	//
	//AdditionalWeaponInfo.Round = AviableAmmoFromInventory;
	//
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);

}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	//���� ������ ��� ������� �������� �� ����� ����, ����� ������������� ������
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false,0);
	DropClipFlag = false;
}



bool AWeaponDefault::CheckCanWeaponReload()
{
	//if owner has no inventory, ammo don't end
	bool result = true;
	if (GetOwner())
	{
		//this weapon has owner  and owner has inventory, 
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		//this  made for cheek can owner reload or not
		if (MyInv)
		{
			int8 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType,AviableAmmoForWeapon))
			{
				result = false;
				MyInv->OnWeaponNotHaveRoundEvent_Multicast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
				//MyInv->OnWeaponNotHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
			else
			{
				MyInv->OnWeaponHaveRoundEvent_Multicast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
				//MyInv->OnWeaponHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
		}
	}
	
	return result;
}

int8 AWeaponDefault::GetAviableAmmoForReload()
{
	int8 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		//this weapon has owner  and owner has inventory, 
		UInventoryComponent* MyInv = Cast<UInventoryComponent>(GetOwner()->GetComponentByClass(UInventoryComponent::StaticClass()));
		//this  made for cheek can owner reload or not
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType,AviableAmmoForWeapon))
			{
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}

void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;
		//��������� ������� ������������ ������
		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		AStaticMeshActor* NewActor = nullptr;

		//for multiplayer
		ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRandomDispersion, PowerImpulse, CustomMass, LocalDir);
	}
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool NewShouidReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouilRadiuceDispersion = NewShouidReduceDispersion;
}

void AWeaponDefault::AnimWeaponStart_Multicast_Implementation(UAnimMontage* AnimFire)
{
	if (AnimFire && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimFire);
	}
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir)
{
	AStaticMeshActor* NewActor = nullptr;

	FActorSpawnParameters Param;
	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	//����� ��������� ������, ��� � ���������
	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, Param);

	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		NewActor->SetActorTickEnabled(false);
		NewActor->InitialLifeSpan = LifeTimeMesh;

		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);
		//���� ���� ��������� ������ ����� ��������� Ũ
		if (CustomMass > 0.f)
		{
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
		}
		//��������� ���� �� ����� ������ �������
		if (!DropImpulseDirection.IsNearlyZero())
		{
			FVector FinalDir;
			LocalDir += (DropImpulseDirection * 1000.f);
			//��������� ��������� ������ ����������� ��������
			if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
			//����������� � ����� ������� ������� ���� ���������
			FinalDir.GetSafeNormal(0.0001f);
			NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
		}
	}
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FxFire, USoundBase* SoundFire)
{
	if (SoundFire)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
	}
	if (FxFire)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FxFire, ShootLocation->GetComponentTransform());
	}
}

void AWeaponDefault::WeaponTraceDecal_Multicast_Implementation(FHitResult TraceHit, UMaterialInterface* TraceDecal)
{
	if (TraceDecal && TraceHit.GetComponent())
	{
		UGameplayStatics::SpawnDecalAttached(TraceDecal, FVector(20.f), TraceHit.GetComponent(), NAME_None, TraceHit.ImpactPoint, TraceHit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 0.f);
	}
}

void AWeaponDefault::WeaponTraceFx_Multicast_Implementation(FHitResult TraceHit, UParticleSystem* TraceFX)
{

	if (TraceFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TraceFX, FTransform(TraceHit.ImpactNormal.Rotation(), TraceHit.ImpactPoint, FVector(1.f)));
	}
	
}

//void AWeaponDefault::WeaponTraceSoundEffectDamage_Multicast_Implementation(FHitResult TraceHit, FProjectileInfo ProjectileInfo, EPhysicalSurface Surface)
//{
//	if (ProjectileInfo.HitSound)
//	{
//		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileInfo.HitSound, TraceHit.ImpactPoint);
//	}
//
//	UTypes::AddEffectBySurfaceType(TraceHit.GetActor(), TraceHit.BoneName, ProjectileInfo.Effect, Surface);
//	//UStateEffect* NewEffect = NewObject<UStateEffect>(Hit.GetActor(), FName("Effect"));
//
//	UGameplayStatics::ApplyPointDamage(TraceHit.GetActor(), ProjectileInfo.ProjectileDamage, TraceHit.TraceStart, TraceHit, GetInstigatorController(), this, NULL);
//}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
	DOREPLIFETIME(AWeaponDefault, WeaponReloading);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);

}
