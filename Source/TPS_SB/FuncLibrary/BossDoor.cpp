// Fill out your copyright notice in the Description page of Project Settings.


#include "BossDoor.h"
#include "Engine/World.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ABossDoor::ABossDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	MeshComponentDoor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door"));
	MeshComponentDoor->SetupAttachment(RootComponent);

	MeshComponentStartCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Start"));
	MeshComponentEndCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("End"));

	MeshComponentStartCollision->SetupAttachment(RootComponent);
	MeshComponentEndCollision->SetupAttachment(RootComponent);

	
}

// Called when the game starts or when spawned
void ABossDoor::BeginPlay()
{
	Super::BeginPlay();

	MeshComponentStartCollision->OnComponentBeginOverlap.AddDynamic(this, &ABossDoor::TimerForDoorUp);
	MeshComponentEndCollision->OnComponentBeginOverlap.AddDynamic(this, &ABossDoor::TimerForDoorDown);

}

// Called every frame
void ABossDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABossDoor::TimerForDoorUp(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	
	GetWorldTimerManager().SetTimer(TimerUP, this, &ABossDoor::DoorUp, 0.1f, true, 0.0f);
}

void ABossDoor::TimerForDoorDown(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherAtor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	GetWorldTimerManager().ClearTimer(TimerUP);
	GetWorldTimerManager().SetTimer(TimerDown, this, &ABossDoor::DoorDown, 0.1f, true, 0.0f);
}

void ABossDoor::DoorUp()
{
	FVector InterpLoc(MeshComponentDoor->GetRelativeLocation());
	FVector NewLoc = FMath::VInterpTo(InterpLoc, FVector(0.0f, 0.0f, 200.0f), 0.1f, 1.0f);
	MeshComponentDoor->SetRelativeLocation(NewLoc);
	bool bResVectorEquals =  MeshComponentDoor->GetRelativeLocation().Equals(FVector(0.0f, 0.0f, 200.0f),0.1f);

	if (bResVectorEquals == true)
	{
		GetWorldTimerManager().ClearTimer(TimerUP);
	}
}

void ABossDoor::DoorDown()
{
	FVector InterpLoc(MeshComponentDoor->GetRelativeLocation());
	FVector NewLoc = FMath::VInterpTo(InterpLoc, FVector(0.0f, 0.0f, 0.0f), 0.1f, 1.0f);
	MeshComponentDoor->SetRelativeLocation(NewLoc);
	bool bResVectorEquals = MeshComponentDoor->GetRelativeLocation().Equals(FVector(0.0f, 0.0f, 0.0f), 0.1f);

	if (bResVectorEquals == true)
	{
		GetWorldTimerManager().ClearTimer(TimerDown);
	}
}

