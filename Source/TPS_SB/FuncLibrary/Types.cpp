// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
// ���� ������ � ����� ���������� ������� ������ ��� ���������� � ����� �������� ��� ����� ��������
#include "TPS_SB/TPS_SB.h"
#include "TPS_SB/Interface/TPS_SB_IGameActor.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{

		UStateEffect* MyEffect = Cast<UStateEffect>(AddEffectClass->GetDefaultObject());
		if (MyEffect)
		{
			bool IsHavePossibleInteructSurface = false;
			int8 i = 0;
			//search effect
			while (i < MyEffect->PossibleInteractSurface.Num() && !IsHavePossibleInteructSurface)
			{
				if (MyEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					IsHavePossibleInteructSurface = true;
					bool bIsCanAddEffect = false;
					if (!MyEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UStateEffect*> CurrentEffects;
						ITPS_SB_IGameActor* MyInterface = Cast<ITPS_SB_IGameActor>(TakeEffectActor);
						if (MyInterface)
						{
							CurrentEffects = MyInterface->GetAllCurrentEffects();
						}
						//check than number of effect > 0
						if (CurrentEffects.Num() > 0)
						{
							//logic add only one type effect
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						//shows than we can add effect
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{

						UStateEffect* NewEffect = NewObject<UStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}
				}
				i++;
			}
		}
		
	}

}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offset, FName Socket)
{
	if (Target)
	{
		FName SocketToAttached = Socket;
		FVector Locat = Offset;
		ACharacter* MyCharacter = Cast<ACharacter>(Target);
		if (MyCharacter && MyCharacter->GetMesh())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFX, MyCharacter->GetMesh(), SocketToAttached, Locat, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			if (Target->GetRootComponent())
			{
				UGameplayStatics::SpawnEmitterAttached(ExecuteFX, Target->GetRootComponent(), SocketToAttached, Locat, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
}
