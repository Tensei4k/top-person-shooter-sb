// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TPS_SB_API UStateEffect : public UObject
{
	GENERATED_BODY()
	
public:
	//This is for ReplicateSubobjects on character, it's unreal need
	//Tell us that this object is a replicated 
	bool IsSupportedForNetworking() const override { return true; };

	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		UParticleSystem* ParticleEffect = nullptr;
		//FName NameOfBone;

		//UParticleSystemComponent* ParticleEmitter = nullptr;
	//this property for say to game is this particle auto destroy or not
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	bool bIsAutoDestroyParticleEffect = false;

	AActor* MyActor = nullptr;
	UPROPERTY(Replicated)
	FName NameBone;
};

UCLASS()
class TPS_SB_API UStateEffect_ExecuteOnce : public UStateEffect
{
	GENERATED_BODY()
public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;

	// My
	FTimerHandle TimerHandle_EffectTimer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
	float RateTime = 1.f;
};

UCLASS()
class TPS_SB_API UStateEffect_ExecuteTimer : public UStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject();

	virtual void Evecute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Timer = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float RateTime = 1.f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};



