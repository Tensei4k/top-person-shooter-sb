// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Granade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
//����� �������
int32 DebugExploseShow = 1;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TPS.DebugExplode"), DebugExploseShow, TEXT("Draw Debug for Explode"), ECVF_Cheat);

// Called when the game starts or when spawned
void AProjectileDefault_Granade::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileDefault_Granade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimeExplose(DeltaTime);

}

void AProjectileDefault_Granade::TimeExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Granade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
															FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Granade::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileDefault_Granade::Explose()
{
	if (DebugExploseShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.f);
	}

	TimerEnabled = true;

	GranadeSoundFx_Multicast(ProjectileSetting.ExploseSound, ProjectileSetting.ExploseFX);

	//if (ProjectileSetting.ExploseFX)
	//{
	//	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	//}
	//if (ProjectileSetting.ExploseSound)
	//{
	//	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	//}
	//����� ������� ��� ��� ������� �����
	TArray<AActor*> IgnoredActor;
	//��������� ������� ����������� �������� ���� �� �����
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSetting.ExploseMaxDamage, ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(), 1000.0f, 2000.0f, 5, NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}

void AProjectileDefault_Granade::GranadeSoundFx_Multicast_Implementation(USoundBase* GranadeSound, UParticleSystem* GranadeFX)
{
	if (GranadeFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), GranadeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (GranadeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), GranadeSound, GetActorLocation());
	}
}

