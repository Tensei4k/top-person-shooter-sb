// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "TimerManager.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BossDoor.generated.h"


class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class TPS_SB_API ABossDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABossDoor();

	FTimerHandle TimerUP;
	FTimerHandle TimerDown;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshComponentDoor;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UBoxComponent* MeshComponentStartCollision;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UBoxComponent* MeshComponentEndCollision;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	UFUNCTION()
		void TimerForDoorUp
		( UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

	UFUNCTION()
		void TimerForDoorDown(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherAtor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

	UFUNCTION()
		void DoorUp();

	UFUNCTION()
		void DoorDown();


};
