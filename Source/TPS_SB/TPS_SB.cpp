// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SB.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS_SB, "TPS_SB" );

DEFINE_LOG_CATEGORY(LogTPS_SB)
DEFINE_LOG_CATEGORY(LogTPS_SB_Net)
