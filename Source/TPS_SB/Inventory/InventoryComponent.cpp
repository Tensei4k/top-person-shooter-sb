// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "TPS_SB/Interface/TPS_SB_IGameActor.h"
#include "TPS_SB/Game/TPS_SBGameInstance.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	//// ...

}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	//���� ������� �� ������� ������ ����������� ������ �� �������
	if (ChangeToIndex > WeaponSlots.Num()-1)
	{
		CorrectIndex = 0;
	}
	else
		if (ChangeToIndex < 0)
		{
			CorrectIndex = WeaponSlots.Num() - 1;
		}
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				bIsSuccess = true;
			}
			else
			{
				//we check all ammo slots and watch are they have ammo by slots
				UTPS_SBGameInstance* MyGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());
				if (MyGI)
				{
					FWeaponInfo MyInfo;
					MyGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, MyInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}
	
	if (!bIsSuccess)
	{
		//if (bIsForward)
		//{
			int8 iteration = 0;
			int8 Seconditeration = 0;
			int8 TmpIndex = 0;
			while (iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				iteration++;
				if (bIsForward)
				{
					TmpIndex = ChangeToIndex + iteration;
				}
				else
				{
					Seconditeration = WeaponSlots.Num() - 1;
					TmpIndex = ChangeToIndex - iteration;
				}
				
				if (WeaponSlots.IsValidIndex(TmpIndex))
				{
					if (!WeaponSlots[TmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[TmpIndex].AdditionalInfo.Round > 0)
						{
							//weapon good
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[TmpIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[TmpIndex].AdditionalInfo;
							NewCurrentIndex = TmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UTPS_SBGameInstance* myGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlots[TmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//weapon good
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[TmpIndex].NameItem;
									NewAdditionalInfo = WeaponSlots[TmpIndex].AdditionalInfo;
									NewCurrentIndex = TmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of right of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//weapon good
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
									NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UTPS_SBGameInstance* myGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//weapon good
											bIsSuccess = true;
											NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
											NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//weapon good
								}
								else
								{
									FWeaponInfo myInfo;
									UTPS_SBGameInstance* myGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//weapon good
											}
											else
											{
												//need init pistol if we will need it
												UE_LOG(LogTemp, Error, TEXT("UInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex - Init PISTOL - NEED"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					if (bIsForward)
					{
						Seconditeration++;
					}
					else
					{
						Seconditeration--;
					}
				}
			}
	//	//}
	//	else
	//	{
	//		int8 iteration = 0;
	//		int8 Seconditeration = WeaponSlots.Num() - 1;
	//		while (iteration < WeaponSlots.Num() && !bIsSuccess)
	//		{
	//			iteration++;
	//			int8 tmpIndex = ChangeToIndex - iteration;
	//			if (WeaponSlots.IsValidIndex(tmpIndex))
	//			{
	//				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
	//				{
	//					if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
	//					{
	//						//weapon good
	//						bIsSuccess = true;
	//						NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
	//						NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
	//						NewCurrentIndex = tmpIndex;
	//					}
	//					else
	//					{
	//						FWeaponInfo myInfo;
	//						UTPS_SBGameInstance* myGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());

	//						myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

	//						bool bIsFind = false;
	//						int8 j = 0;
	//						while (j < AmmoSlots.Num() && !bIsFind)
	//						{
	//							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
	//							{
	//								//weapon good
	//								bIsSuccess = true;
	//								NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
	//								NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
	//								NewCurrentIndex = tmpIndex;
	//								bIsFind = true;
	//							}
	//							j++;
	//						}
	//					}
	//				}
	//			}
	//			else
	//			{
	//				//go to end of left of array weapon slots
	//				if (OldIndex != Seconditeration)
	//				{
	//					if (WeaponSlots.IsValidIndex(Seconditeration))
	//					{
	//						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
	//						{
	//							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
	//							{
	//								//WeaponGood
	//								bIsSuccess = true;
	//								NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
	//								NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
	//								NewCurrentIndex = Seconditeration;
	//							}
	//							else
	//							{
	//								FWeaponInfo myInfo;
	//								UTPS_SBGameInstance* myGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());

	//								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

	//								bool bIsFind = false;
	//								int8 j = 0;
	//								while (j < AmmoSlots.Num() && !bIsFind)
	//								{
	//									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
	//									{
	//										//weapon good
	//										bIsSuccess = true;
	//										NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
	//										NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
	//										NewCurrentIndex = Seconditeration;
	//										bIsFind = true;
	//									}
	//									j++;
	//								}
	//							}
	//						}
	//					}
	//				}
	//				else
	//				{
	//					//go to same weapon when start
	//					if (WeaponSlots.IsValidIndex(Seconditeration))
	//					{
	//						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
	//						{
	//							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
	//							{
	//								//weapon good
	//							}
	//							else
	//							{
	//								FWeaponInfo myInfo;
	//								UTPS_SBGameInstance* myGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());

	//								myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

	//								bool bIsFind = false;
	//								int8 j = 0;
	//								while (j < AmmoSlots.Num() && !bIsFind)
	//								{
	//									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
	//									{
	//										if (AmmoSlots[j].Cout > 0)
	//										{
	//											//weapon good
	//										}
	//										else
	//										{
	//											//need init pisto; if we will need it
	//											UE_LOG(LogTemp, Error, TEXT("UInventoryComponent::SwitchWeaponToIndexByNextPreviosIndex - Init PISTOL - NEED"));
	//										}
	//									}
	//									j++;
	//								}
	//							}
	//						}
	//					}
	//				}
	//				Seconditeration--;
	//			}
	//		}
	//	}
	}
	//���� ��� ������ �������
	if (bIsSuccess)
	{
		//���������� ����� ������ ������� ��������
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeaponEven_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		//OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex); 
	}

	return bIsSuccess;
}

bool UInventoryComponent::SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAdditionalWeaponInfo PreviosWeaponInfo)
{
	bool bIsSuccess = false;
	FName ToSwitchIdWeapon;
	FAdditionalWeaponInfo ToSwitchAdditionalInfo;

	ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
	ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAdditionalInfoWeapon(PreviosIndex, PreviosWeaponInfo);
		OnSwitchWeaponEven_OnServer(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);
		//OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

		//Check Ammo slot for event to player
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int8 AviableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AviableAmmoForWeapon))
			{
			}
		}
		bIsSuccess = true;
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if ( i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPS_SBInventoruComponent::SetAdditionalInfoWeapon - No Found Weapon With index - %d"), IndexWeapon);
		}
	}
	else
				UE_LOG(LogTemp, Warning, TEXT("UTPS_SBInventoruComponent::SetAdditionalInfoWeapon - Not Correct Weapon index - %d"), IndexWeapon);
	return result;
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i<WeaponSlots.Num()&&!bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

FName UInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName Result;

	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		Result = WeaponSlots[IndexSlot].NameItem;
	}
	return Result;
}

bool UInventoryComponent::GetWeaponTypeByIndexSlot(int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTPS_SBGameInstance* MyGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			MyGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}

	return bIsFind;
}

bool UInventoryComponent::GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	FWeaponInfo OutInfo;
	WeaponType = EWeaponType::RifleType;
	UTPS_SBGameInstance* MyGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		MyGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}

	return bIsFind;
}

void UInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i<WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot*/ i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				OnWeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
				//OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon With index - %d"), IndexWeapon);
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::SetAdditionalInfoWeapon - Not Correct Weapon index - %d"), IndexWeapon);
}

void UInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i<AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}
			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			//OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			bIsFind = true;
		}
		i++;
	}
}

bool UInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AviableAmmoWeapon)
{
	AviableAmmoWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i<AmmoSlots.Num() && !bIsFind)
	{
		//if type of weapon slot type equals weapon type
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoWeapon = AmmoSlots[i].Cout;
			//check projectile more zero
			if (AmmoSlots[i].Cout > 0)
			{
				return true;
			}
		}
		i++;
	}
	if (AviableAmmoWeapon <= 0)
	{
		OnWeaponAmmoEmptyEvent_Multicast(TypeWeapon);
		//OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
	}
	else
	{
		OnWeaponAmmoAviableEvent_Multicast(TypeWeapon);
		//OnWeaponAmmoAviable.Broadcast(TypeWeapon);
	}

	return false;
}

bool UInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i<AmmoSlots.Num() && !result)
	{
		//check on type and ammo less than need we can take this ammo
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
		{
			result = true;
		}
		i++;
	}
	return true;
}

bool UInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i<WeaponSlots.Num() && !bIsFreeSlot)
	{
		//if we found at leas one free slot, we can take weapon
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool Result = false;
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		//repair weapon
		WeaponSlots[IndexSlot] = NewWeapon;
		SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo,true);
		//accept slot when we change and new inform to weapon
		OnUpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);
		//OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);

		Result = true;
	}
	return Result;
}

void UInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor, FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;

			OnUpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);
			//OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);

			if (PickUpActor)
			{
				PickUpActor->Destroy();
			}
		}		
	}
}

void UInventoryComponent::DropWeaponByIndex_OnServer_Implementation(int32 ByIndex)
{
	FDropItem DropItemInfo;
	FWeaponSlot EmptyWeaponSlot;

	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AviableWeaponNum = 0;
	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			AviableWeaponNum++;
			if (AviableWeaponNum > 1)
			{
				bIsCanDrop = true;
			}
		}
		i++;
	}

	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		bool bIsFindWeapon = true;
		int8 j = 0;
		while (j < WeaponSlots.Num() && !bIsFindWeapon)
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				OnSwitchWeaponEven_OnServer(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
				//OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
			}
			j++;
		}

		WeaponSlots[ByIndex] = EmptyWeaponSlot;
		if (GetOwner()->GetClass()->ImplementsInterface(UTPS_SB_IGameActor::StaticClass()))
		{
			ITPS_SB_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}
		OnUpdateWeaponSlotsEvent_Multicast(ByIndex, EmptyWeaponSlot);
		//OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot);
	}
}

bool UInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo)
{
	bool Result = false;
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UTPS_SBGameInstance* MyGI = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		Result = MyGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		//WRITE TO OUT WEAPON CORRECT NUMNER OF AMMO
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}
	return Result;
}

TArray<FWeaponSlot> UInventoryComponent::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UInventoryComponent::GetAmmoSlots()
{
	return AmmoSlots;
}

void UInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoAlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoAlotsInfo;
	//old logic not is is in player state
	//for (int8 i = 0; i < WeaponSlots.Num(); i++)
	//{
	//	UTPS_SBGameInstance* MyGi = Cast<UTPS_SBGameInstance>(GetWorld()->GetGameInstance());
	//	if (MyGi)
	//	{
	//		if (!WeaponSlots[i].NameItem.IsNone())
	//		{
	//			//FWeaponInfo Info;
	//			//if (MyGi->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
	//			//{
	//			//	WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
	//			//}
	//		}
	//	}
	//}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeaponEven_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
			//OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}

void UInventoryComponent::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeWeapon, int32 Cout)
{
	OnAmmoChange.Broadcast(TypeWeapon, Cout);

}

void UInventoryComponent::OnSwitchWeaponEven_OnServer_Implementation(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	OnSwitchWeapon.Broadcast(WeaponIdName, WeaponAdditionalInfo, NewCurrentIndexWeapon);
}

void UInventoryComponent::OnWeaponAdditionalInfoChangeEvent_Multicast_Implementation(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UInventoryComponent::OnWeaponAmmoEmptyEvent_Multicast_Implementation(EWeaponType WeaponType)
{
	OnWeaponAmmoEmpty.Broadcast(WeaponType);
}

void UInventoryComponent::OnWeaponAmmoAviableEvent_Multicast_Implementation(EWeaponType WeaponType)
{
	OnWeaponAmmoAviable.Broadcast(WeaponType);
}

void UInventoryComponent::OnUpdateWeaponSlotsEvent_Multicast_Implementation(int32 IndexSlotChange, FWeaponSlot NewInfo)
{
	OnUpdateWeaponSlots.Broadcast(IndexSlotChange, NewInfo);
}

void UInventoryComponent::OnWeaponNotHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
	OnWeaponNotHaveRound.Broadcast(IndexSlotWeapon);
}

void UInventoryComponent::OnWeaponHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
	OnWeaponHaveRound.Broadcast(IndexSlotWeapon);
}

//Give possibility to replication 
void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UInventoryComponent, AmmoSlots);
	

}

