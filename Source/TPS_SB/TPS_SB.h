// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTPS_SB, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogTPS_SB_Net, Log, All);
