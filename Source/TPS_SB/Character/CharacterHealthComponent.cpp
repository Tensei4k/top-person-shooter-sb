// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthComponent.h"

void UCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{

float CurrentDamage = ChangeValue * CoefDamage;

if (Shield > 0.0f && ChangeValue < 0.0f)
{
	ChangeShieldValue(ChangeValue);
	if (Shield<0.0f)
	{
		//Add FX 
		UE_LOG(LogTemp, Warning, TEXT("UCharacterHealthComponent::ChangeHealthValue - Shield < 0"));
	}
}
else
{
	Super::ChangeHealthValue_OnServer(ChangeValue);
}


}

float UCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	ShieldChangeEvent_Multicast(Shield, ChangeValue);
	//OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield>100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield<0.0f)
		{
			Shield = 0.0f;
		}
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

}

void UCharacterHealthComponent::CoolDownShieldEnd()
{

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UCharacterHealthComponent::RecoveryShield()
{
	float Tmp = Shield;
	Tmp = Tmp + ShieldRecoverValue;
	if (Tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}

	}
	else
	{
		Shield = Tmp;
	}

	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

float UCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float NewDamage)
{
	OnShieldChange.Broadcast(NewShield, NewDamage);
}
