// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool UHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void UHealthComponent::ChangeHealthValue_OnServer_Implementation(float ChangeValue)
{
	if (bIsAlive)
	{
		ChangeValue = ChangeValue * CoefDamage;
	Health += ChangeValue;

	OnHealthChangeEvent_Multicast(Health, ChangeValue);
	//OnHealthChange.Broadcast(Health, ChangeValue);

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			bIsAlive = false;
			OnDeadEvent_Multicast();
			//OnDead.Broadcast();
		}
	}


	}
	
}

void UHealthComponent::OnHealthChangeEvent_Multicast_Implementation(float NewHealth, float NewValue)
{
	OnHealthChange.Broadcast(NewHealth, NewValue);
}

void UHealthComponent::OnDeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();

}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UHealthComponent, Health);
	DOREPLIFETIME(UHealthComponent, bIsAlive);
}
