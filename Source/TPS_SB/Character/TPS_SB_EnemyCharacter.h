// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS_SB/Interface/TPS_SB_IGameActor.h"
#include "TPS_SB_EnemyCharacter.generated.h"

UCLASS()
class TPS_SB_API ATPS_SB_EnemyCharacter : public ACharacter, public ITPS_SB_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATPS_SB_EnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UStateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UStateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UStateEffect* NewEffect);
	void AddEffect_Implementation(UStateEffect* NewEffect) override;

	//Effect
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UStateEffect*> Effects;

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteAddParticle);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteAddParticle);

	UFUNCTION()
		void SwitchEffect(UStateEffect* Effect, bool bIsAdd);
};
