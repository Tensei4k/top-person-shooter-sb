// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS_SB/FuncLibrary/Types.h"
#include "TPS_SB/FuncLibrary/WeaponDefault.h"
#include "TPS_SB/Inventory/InventoryComponent.h"
#include "TPS_SB/Interface/TPS_SB_IGameActor.h"
#include "TPS_SB/FuncLibrary/StateEffect.h"
#include "CharacterHealthComponent.h"
#include "TPS_SBCharacter.generated.h"


UCLASS(Blueprintable)
class ATPS_SBCharacter : public ACharacter, public ITPS_SB_IGameActor
{
	GENERATED_BODY()

protected:
	bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void BeginPlay() override;

	// Inputs

	void InputAxisX(float Value);
	void InputAxisY(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	//Inventory inputs
	UFUNCTION(Server, Reliable)
	void TrySwitchNextWeapon_OnServer();
	UFUNCTION(Server, Reliable)
	void TrySwitchPreviosWeapon_OnServer();
	//Ability inputs
	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}
	//Inputs End

	//Inputs flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnabled = false;
	UPROPERTY(EditAnywhere,  BlueprintReadWrite, Category = "Movement")
	bool AimEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//bool bIsAlive = true;
	//Maybe need only Replicated
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	EMovementState MovementState = EMovementState::Run_State;
	//Weapon
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UDecalComponent* CurrentCursor = nullptr;
	//Effects
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Effects")
	TArray<UStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UStateEffect* EffectRemove = nullptr;

	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

		
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	ATPS_SBCharacter();
	//My realisation camera move
	FTimerHandle TimerHandleTPS; 

	FTimerHandle TimerHandle_RagdollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	//������� ��������� ������������� ����� ������
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement") 
		bool SprintRunPressedEnabled = false;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		class UInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		class UCharacterHealthComponent* HealthComponent;

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		TArray<UAnimMontage*> DeadsAnim;

	//effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UStateEffect> AbilityEffect;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	//My realisation camera move
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HieghtCameraMin = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HieghtCameraMax = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HieghtCameraChangeDistance = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float TimerStep = 0.001f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HieghtCameraChangeSideStepDistance = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float CurrentSideeDistance = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		bool bSideUP = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		bool bSideDone = true;

	//My
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
		FName NameOfBone;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
		FVector LocSpawnEffect;
	//My realisation camera move
	UFUNCTION()
		void InputMouseWheel(float Value);
	//My realisation camera move
	float AxisMouseWheel = 0.0f;
	// Tick Func
	UFUNCTION()
		void MovementTick(float DelataTime);
	UFUNCTION()
		void CameraSmoothSide(float DelataTime);
	UFUNCTION()
		void CameraSmoothSideTick();

	UFUNCTION()
		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void SprintForward();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess,int32 AmmoSafe);
	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);
	void DropCurrentWeapon();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);


	UFUNCTION(BlueprintCallable, BlueprintPure)
		UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState(); //Maybe this is relisation Home work
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UStateEffect*> GetCurrentEffectsOnChar(); //Maybe this is relisation Home work
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex(); //Maybe this is relisation Home work

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UStateEffect*> GetAllCurrentEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UStateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UStateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UStateEffect* NewEffect);
	void AddEffect_Implementation(UStateEffect* NewEffect) override;


	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Muilticast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* Anim);
	// name of function copy macros property EffectAdd
	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteAddParticle);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteAddParticle);

	UFUNCTION()
	void SwitchEffect(UStateEffect* Effect, bool bIsAdd);
};