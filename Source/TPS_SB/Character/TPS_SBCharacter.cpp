// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_SBCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TimerManager.h"
#include "TPS_SB/Game/TPS_SBGameInstance.h"
#include "Engine/World.h"
#include "TPS_SB/FuncLibrary/ProjectileDefault.h"
#include "TPS_SB/TPS_SB.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"

ATPS_SBCharacter::ATPS_SBCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	//������� �����������, ��� ���� ���, ����� ������� Character? ����� inventory
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("HealthComponent"));

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATPS_SBCharacter::CharDead);
	}

	if (InventoryComponent)
	{ 
		//����� Broadcat �������� ���� �� ��������� 
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPS_SBCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//NetCode don't forget for multyplayer
	bReplicates = true;
}

void ATPS_SBCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* MyPC = Cast<APlayerController>(GetController());
		if (MyPC && MyPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			MyPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			//��������� ������� �� ������ ������� � �������
			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}


	MovementTick(DeltaSeconds);
	CameraSmoothSide(DeltaSeconds);
	SprintForward();
	

}

void ATPS_SBCharacter::BeginPlay()
{
	Super::BeginPlay();
	//create for net code we check that this net mode don't remote server
	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		//��������. ���� ���� �������� ��� �������, ������ ���������
		//chec that the cleient on the character
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}

}

//��������� ��������
void ATPS_SBCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	//������ ��� �� ��� ����, �������� �������, � �� ��������� � ���
	Super::SetupPlayerInputComponent(NewInputComponent);
	//����������� ������ ����������
	NewInputComponent->BindAxis(FName("MoveForward"), this, &ATPS_SBCharacter::InputAxisX);
	NewInputComponent->BindAxis(FName("MoveRight"), this, &ATPS_SBCharacter::InputAxisY);
	NewInputComponent->BindAxis(FName("MouseWheelCameraTPS"), this, &ATPS_SBCharacter::InputMouseWheel);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPS_SBCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPS_SBCharacter::InputAttackReleased);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPS_SBCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATPS_SBCharacter::TrySwitchNextWeapon_OnServer);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATPS_SBCharacter::TrySwitchPreviosWeapon_OnServer);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATPS_SBCharacter::TryAbilityEnabled);
	// Maybe need change DropCurrentWeapon to InteractPickUpActor than swap weapon need to see lessons
	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATPS_SBCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATPS_SBCharacter::TKeyPressed<0>);
}

void ATPS_SBCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPS_SBCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}
void ATPS_SBCharacter::InputMouseWheel(float Value)
{
	AxisMouseWheel = Value;
}
void ATPS_SBCharacter::InputAttackPressed()
{
	if (HealthComponent && HealthComponent-GetIsAlive())
	{
		AttackCharEvent(true);
	}
}
void ATPS_SBCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPS_SBCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATPS_SBCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATPS_SBCharacter::InputSprintPressed()
{
	SprintRunEnabled = true;
	ChangeMovementState();
}

void ATPS_SBCharacter::InputSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
}

void ATPS_SBCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATPS_SBCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

//������� �� ��, ���������� ���������.
void ATPS_SBCharacter::MovementTick(float DelataTime)
{
	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SEnum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogTPS_SB_Net, Warning, TEXT("Movement state - %s"), *SEnum);

			if (MovementState == EMovementState::SprintRun_State)
			{
				FVector MyRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator MyRotator = MyRotationVector.ToOrientationRotator();

				SetActorRotation((FQuat(MyRotator)));
				SetActorRotationByYaw_OnServer(MyRotator.Yaw);
			}
			else
			{
				APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (MyController)
				{
					FHitResult ResultHit;
					//MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
					//�� �������� ��� ECC_GameTraceChannel1 ��� Enum � �� ���������� �����
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
					// �������� FindLookAtRotation, ����� ����� ���� ����������������� � ����������, � ������ ��� ������ ��������� ��������� ������ SetActorRotation
					float  FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);
					if (CurrentWeapon)
					{
						FVector Displacment = FVector(0);
						bool bIsReduceDispertion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacment = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouilRadiuceDispersion = true;
							bIsReduceDispertion = true;
							break;
						case EMovementState::AimWalk_State:
							Displacment = FVector(0.0f, 0.0f, 1160.0f);
							//CurrentWeapon->ShouilRadiuceDispersion = true;
							bIsReduceDispertion = true;
							break;
						case EMovementState::Walk_State:
							Displacment = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouilRadiuceDispersion = false;
							break;
						case EMovementState::Run_State:
							Displacment = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouilRadiuceDispersion = false;
							break;
						case EMovementState::SprintRun_State:
							break;
						default:
							break;

						}
						// ���������  ����� ���� ������������ �������
						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacment;
						//ResultHit.Location + Displacment is point where projectile stop
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacment, bIsReduceDispertion);
					}
				}
			}
		}

		
	}
	
}
//����������� ��������� ������
void ATPS_SBCharacter::CameraSmoothSide(float DelataTime)
{
	if (IsLocallyControlled())
	{
		if (bSideDone == true)
		{
			if (AxisMouseWheel > 0 && CameraBoom->TargetArmLength >= HieghtCameraMin)
			{
				CameraBoom->TargetArmLength = CameraBoom->TargetArmLength - HieghtCameraChangeDistance;
				bSideUP = false;
				bSideDone = false;

			}
			if (AxisMouseWheel < 0 && CameraBoom->TargetArmLength <= HieghtCameraMax)
			{
				CameraBoom->TargetArmLength = CameraBoom->TargetArmLength + HieghtCameraChangeDistance;
				bSideUP = true;
				bSideDone = false;
			}
			GetWorldTimerManager().SetTimer(TimerHandleTPS, this, &ATPS_SBCharacter::CameraSmoothSideTick, TimerStep, true, 0.0f);
		}
	}
	
}
//����������� �����������/��������� ������ ����� ������
void ATPS_SBCharacter::CameraSmoothSideTick()
{
	CurrentSideeDistance = CurrentSideeDistance + HieghtCameraChangeSideStepDistance;
	if (bSideUP == false)
	{
		CameraBoom->TargetArmLength = CameraBoom->TargetArmLength - HieghtCameraChangeSideStepDistance;
	}
	else
	{
		CameraBoom->TargetArmLength = CameraBoom->TargetArmLength + HieghtCameraChangeSideStepDistance;
	}
	if (CurrentSideeDistance >= HieghtCameraChangeSideStepDistance)
	{
		CurrentSideeDistance = 0.0f;
		bSideDone = true;
		GetWorldTimerManager().ClearTimer(TimerHandleTPS);
	}
}

void ATPS_SBCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* MyWeapon = nullptr;
	//���������� ������ � ���� ��� ���� ��������� ������ ������
	MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp,Warning,TEXT("ATPS_SBCharacter::AttackCharEvent - CurrentWeapon- NULL"));
	}
}

void ATPS_SBCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}
//���������
void ATPS_SBCharacter::SprintForward()
{
	
	FVector Velocity = GetVelocity();
	Velocity.Normalize();

	FVector ForwardVector = GetActorForwardVector();
	ForwardVector.Normalize();
	
	if (Velocity.Equals(ForwardVector,0.3) && SprintRunPressedEnabled)
	{
		if (!SprintRunEnabled) 
		{
			SprintRunEnabled = true;
			ChangeMovementState();
		}
	}
	else
	{
		if (SprintRunEnabled)
		{
			SprintRunEnabled = false;
			ChangeMovementState();
		}
	}
	

}
//��������� ��������� ��������
void ATPS_SBCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::Run_State;
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::SprintRun_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			{
				NewState = EMovementState::AimWalk_State;
			}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_State;
				}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}
		}
	}

	SetMovementState_OnServer(NewState);

	//CharacterUpdate();
	//�������� �������� ��������
	AWeaponDefault* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->UpdateStateWeapon_OnServer(NewState);
	}

}
// ������� �������, � ������� ����� �������� ������ �� ����������, 
// ��� ���� ����� � ��������� ������ ���� ������ �������� � ���������, ����� ����� ������ �� ���������
AWeaponDefault* ATPS_SBCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}
// ������������� ������
void ATPS_SBCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	//�������� �� ������ ��� �����,  � ����� ���������� � ����������� ������
	//OnServer
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	UTPS_SBGameInstance* MyGI = Cast<UTPS_SBGameInstance>(GetGameInstance());
	FWeaponInfo MyWeaponInfo;
	if (MyGI)
	{
		if (MyGI->GetWeaponInfoByName(IdWeaponName, MyWeaponInfo))
		{//��������� ��� ���������� �� ������
			if (MyWeaponInfo.WeaponClass)
			{
				//��������� ������
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				//������� ��� �� �������� ����������� ��� �������
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				//�������� ��������� �������
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();
				//�������� ��������� ������ ������������ SpawnActor
				AWeaponDefault* MyWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				//�������� ��� ���������� ��������� � � ��� ��� �� ����
				if (MyWeapon)
				{
					//���� ������� ���� ���������� ����� enum ��������
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					//���������� � ��������� �� ������ �������, ������� ������� ��� ��������
					CurrentWeapon = MyWeapon;

					MyWeapon->IdWeaponName = IdWeaponName; //This property don't exist
					//������ ���, ����� ��������� ������ �� ������ ���������, �� � ������ � �����
					MyWeapon->WeaponSetting = MyWeaponInfo;
					//MyWeapon->AdditionalWeaponInfo.Round = MyWeaponInfo.MaxRound;
					//������� �������� ��� ������
					MyWeapon->ReloadTime = MyWeaponInfo.ReloadTime;
					MyWeapon->UpdateStateWeapon_OnServer(MovementState);
					MyWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					//if (InventoryComponent)
					CurrentIndexWeapon = NewCurrentIndexWeapon;
					//����� Broadcast, ����� ������� ����, ��� ��������������, ����� ������ ������� ������� Character �������
					MyWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPS_SBCharacter::WeaponReloadStart);
					MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPS_SBCharacter::WeaponReloadEnd);
					MyWeapon->OnWeaponFireStart.AddDynamic(this, &ATPS_SBCharacter::WeaponFireStart);

					//if we hasn't ammo in weapon, after switch to this weapon init reload
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}
					if (InventoryComponent)
					{
						//tell that we switch weapon and we have enough ammo
						InventoryComponent->OnWeaponAmmoAviableEvent_Multicast(MyWeapon->WeaponSetting.WeaponType);
						//InventoryComponent->OnWeaponAmmoAviable.Broadcast(MyWeapon->WeaponSetting.WeaponType);
					}
				}

			}
			
		}
		else
			{
				UE_LOG(LogTemp, Warning, TEXT("ATPS_SBCharacter::InitWeapon - Weapon Not Found in table - NULL"));
			}
	}
	
}

void ATPS_SBCharacter::TryReloadWeapon()
{
		//���� ������ � ���������� �� ������������
		if (HealthComponent && HealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
		{
			TryReloadWeapon_OnServer();
		}
}

void ATPS_SBCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATPS_SBCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponFireStart_BP(Anim);
}

void ATPS_SBCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{	//change ammo
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	//������ ������� ��� ������ � ����������, �� �������� _Implementation
	WeaponReloadEnd_BP(bIsSuccess);
}

void ATPS_SBCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;
			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
			}
			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

void ATPS_SBCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATPS_SBCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void ATPS_SBCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{

}

void ATPS_SBCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
}

UDecalComponent* ATPS_SBCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

EMovementState ATPS_SBCharacter::GetMovementState()
{
	return MovementState;
}

TArray<UStateEffect*> ATPS_SBCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

int32 ATPS_SBCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

bool ATPS_SBCharacter::GetIsAlive()
{
	bool Result = false;
	if (HealthComponent)
	{
		Result = HealthComponent->GetIsAlive();
	}
	return Result;
}

void ATPS_SBCharacter::TrySwitchNextWeapon_OnServer_Implementation()
{
	//��������� ���� �� ���� �� 2 ����� � ���������
	if(CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)// (InventoryComponent->WeaponSlots.Num()>1)
	{
		//��������� ������ �������� ������
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		// ����� ������� ������
		if (CurrentWeapon)
		{
			//�������� ���� ������� ���� ��������
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			//���� ������ �������������. ������ ������ ����������
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		//����������, ��� ���� ��������� ���������
		if (InventoryComponent)
		{
			//������ �������� �� �������
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATPS_SBCharacter::TrySwitchPreviosWeapon_OnServer_Implementation()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)//(InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}


void ATPS_SBCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)
	{
		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

EPhysicalSurface ATPS_SBCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default; 
	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* MyMaterial = GetMesh()->GetMaterial(0);
				if (MyMaterial)
				{
					Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UStateEffect*> ATPS_SBCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_SBCharacter::RemoveEffect_Implementation(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
	
}

void ATPS_SBCharacter::AddEffect_Implementation(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}

//	return UGameplayStatics::SpawnEmitterAttached(NewEffect->ParticleEffect, GetMesh(),
	//	NameOfBone, LocSpawnEffect, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);;
}

void ATPS_SBCharacter::CharDead_BP_Implementation()
{
	//BP
}

void ATPS_SBCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATPS_SBCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATPS_SBCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Muilticast(NewState);
}

void ATPS_SBCharacter::SetMovementState_Muilticast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ATPS_SBCharacter::TryReloadWeapon_OnServer_Implementation()
{
	//���� �������� ���������� �������� ������ ��� ����� , ��� �� ������� ������ ����� ��������� ��������
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->InitReload();
	}
}

void ATPS_SBCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void ATPS_SBCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);

	}
}

void ATPS_SBCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPS_SBCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteAddParticle)
{
	ExecuteEffectAdded_Multicast(ExecuteAddParticle);
}

void ATPS_SBCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteAddParticle)
{
	UTypes::ExecuteEffectAdded(ExecuteAddParticle, this, FVector(0), FName("head"));

}

void ATPS_SBCharacter::SwitchEffect(UStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttack = Effect->NameBone;
			FVector Locat = FVector(0);
			USkeletalMeshComponent* MyMesh = GetMesh();
			if (MyMesh)
			{
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MyMesh, NameBoneToAttack, Locat, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsfind = false;
			if (ParticleSystemEffects.Num() > 0)
			{
				while (i < ParticleSystemEffects.Num() && !bIsfind)
				{
					if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsfind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}

		}

	}
}


void ATPS_SBCharacter::CharDead()
{
	CharDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		int32  Rnd = FMath::RandHelper(DeadsAnim.Num());

		if (DeadsAnim.IsValidIndex(Rnd) && DeadsAnim[Rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			//I'm added -1 to make normal death
			TimeAnim = DeadsAnim[Rnd]->GetPlayLength() - 1;
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[Rnd]);
			PlayAnim_Multicast(DeadsAnim[Rnd]);
		}

		//HealthComponent->GetIsAlive();

		if (GetController())
		{
			GetController()->UnPossess();
		}

		//UnPossessed();

		GetWorldTimerManager().SetTimer(TimerHandle_RagdollTimer, this, &ATPS_SBCharacter::EnableRagdoll_Multicast, TimeAnim, false);

		SetLifeSpan(20.f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.f);
		}

		//GetWorld()->GetAuthGameMode(); //Maybe this isn't need
	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}

		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}


}

void ATPS_SBCharacter::EnableRagdoll_Multicast_Implementation()
{

	if (GetMesh())
	{
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATPS_SBCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		HealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}
	//add Effect to grenade damage
	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* MyProjectile = Cast <AProjectileDefault>(DamageCauser);
		if (MyProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, MyProjectile->ProjectileSetting.Effect, GetSurfaceType()); //To do NAME_None Bone for Radial damage
		}
	}
	
	return ActualDamage;
}

bool ATPS_SBCharacter::ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i< Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPS_SBCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATPS_SBCharacter, MovementState);
	DOREPLIFETIME(ATPS_SBCharacter, CurrentWeapon);
	DOREPLIFETIME(ATPS_SBCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATPS_SBCharacter, Effects);
	DOREPLIFETIME(ATPS_SBCharacter, EffectAdd);
	DOREPLIFETIME(ATPS_SBCharacter, EffectRemove);
}

